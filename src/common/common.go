// ONLY PUT FUNCTIONS / VARS THAT WILL BE USED BY MANY PACKAGES
package common

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// Every respose MUST be a single string.
type response struct {
	Mssg string
}

// Builds and write json response given http status code, and message.
func JsonRespond(w *http.ResponseWriter, rc int, body string) {
	res1D := &response{Mssg: body}
	res1B, _ := json.Marshal(res1D)
	(*w).WriteHeader(rc)
	log.Printf("RC: %d | Message: %s", rc, body)
	fmt.Fprintln(*w, string(res1B))
}

// Assign a handler function to an endpoint.
func MakeHTTPHandler(route string, h http.HandlerFunc) http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/"+route, h)
	r.HandleFunc("/"+route+"/", h)
	return r
}
