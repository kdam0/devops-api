package main

import (
	"app/common"
	"app/jobCheckin"
	"log"
	"net/http"
	"os"
)

func pingHandler(w http.ResponseWriter, r *http.Request) {
	common.JsonRespond(&w, http.StatusOK, "pong")
	return
}

func main() {
	// define urls being served
	pingEndpoint := "ping"
	checkinEndpoint := "check-in"

	// create mux
	mux := http.NewServeMux()

	// define handlers for endpoints here
	mux.HandleFunc("/"+pingEndpoint, pingHandler)
	mux.HandleFunc("/"+pingEndpoint+"/", pingHandler)

	// check-in handled by jobCheckin package
	jobCheckinHandler := jobCheckin.MakeHTTPHandler(checkinEndpoint)
	mux.Handle("/"+checkinEndpoint, jobCheckinHandler)
	mux.Handle("/"+checkinEndpoint+"/", jobCheckinHandler)

	// add more routes... eg. /foo handled by foo package
	// ...

	log.Println("Starting Restful services...")
	log.Println("Using port:", os.Getenv("HTTP_LISTEN_PORT"))

	// start listening
	err := http.ListenAndServe(":"+os.Getenv("HTTP_LISTEN_PORT"), mux)
	log.Print(err)
	errorHandler(err)
}

func errorHandler(err error) {
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
