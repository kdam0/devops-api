# dvo-api
This project is structured in a way so that development + execution is as easy as possible (using docker), while still making the code as modular as possible.
For eg., nothing extra needs to be done to import 3rd party libraries - simply add it to the imports in the applicable file, and docker will handle the rest duing the image build.

## Project details
See the [confluence page](https://confluence.acuityads.com/display/ITOPS/Devops+Monitor+API).

## General rules
1. Each new functionality (endpoint) should be a new package in `src/` under a package sub-dir with the **same name as the package itself**.
2. To maintain **styling consistency**, all *.go* files must be run through `go fmt` prior to making a PR. See https://blog.golang.org/gofmt for a guide.

## Dependencies
1. docker

## Running locally
1. Clone the repo etc.
2. Build and run: `docker-compose build && docker-compose up -d --force-recreate`

## Update the K8S deployment
Suppose you have made code updates and want to deploy these updates into k8s, follow these steps:
1. Updated docker image (acuitydevops/dvo-api:latest) and push to dockerhub - rebuild_image.sh
2. Restart the deployment: `kubectl rollout restart deployment/dvo-api -n devops-scheduler`. Since we set the image pull setting to "Always" in the deployment spec, the updated image should be pulled in the pod recreation - restart_service.sh
