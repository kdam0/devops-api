#!/bin/sh

docker rmi acuitydevops/dvo-api

docker image build --tag acuitydevops/dvo-api .

docker --config /root/.acuitydevops-docker push acuitydevops/dvo-api:latest
