FROM golang:latest

LABEL MAINTAINER=AcuityAds-DevOps
ENV TZ=America/Toronto

# Get a package manager
RUN go get github.com/golang/dep/cmd/dep

WORKDIR /go/src/app

# add source code
ADD src/ ./

RUN dep init

# install dependencies
#RUN dep status
RUN dep ensure --vendor-only

#RUN go build -o main .
CMD ["go", "run", "app.go"]
